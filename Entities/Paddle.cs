using static Game.GlobalData;

namespace Game.Entities
{
    public enum Sides { Left, Right }

    public class Paddle : Entity
    {
        public int Width { get; }
        public int Height { get; }
        public Sides Side { get; }
        public static int Speed { get; } = 3;

        public Paddle(int x, int y, int width, int height, Sides side) : base(x, y)
        {
            Width = width;
            Height = height;
            Side = side;
        }

        public void HandleCollision()
        {
            if (Y <= 0) { Y = 0; }
            else if (Y >= WindowHeight - Height) { Y = WindowHeight - Height; }
        }
    }
}
