using Raylib_cs;
using System.IO;

using static Raylib_cs.Raylib;
using static Game.GlobalData;

namespace Game.Entities
{
    public class Ball : Entity
    {
        public int Radius { get; }

        private int VelocityX { get; set; } = (r.Next(2) == 1) ? 4 : -4;
        private int VelocityY { get; set; } = 3;

        private Sound _soundBounce = LoadSound(Path.Combine(SoundsPath, "bounce.wav"));
        private Sound _soundPoint = LoadSound(Path.Combine(SoundsPath, "point.wav"));

        private static readonly System.Random r = new();

        public Ball(int radius, int x = 0, int y = 0) : base(x, y)
        {
            Radius = radius;
        }

        public void Reset()
        {
            X = (WindowWidth / 2) - Radius;
            Y = (WindowHeight / 2) - Radius;

            VelocityX = -VelocityX;
            VelocityY = -VelocityY;
        }

        public void Move()
        {
            X += VelocityX;
            Y += VelocityY;
        }

        public void HandleAreaCollision()
        {
            if (X - Radius <= 0) { X = Radius; ReverseVelocityX(); }
            if (Y - Radius <= 0) { Y = Radius; ReverseVelocityY(); }
            if (X + Radius >= WindowWidth) { X = WindowWidth - Radius; ReverseVelocityX(); }
            if (Y + Radius >= WindowHeight) { Y = WindowHeight - Radius; ReverseVelocityY(); }
        }

        // Calculates current collision boxes on the front and the sides of the paddle and responds to ball's interaction with them appropriately
        public void HandleCollisionWithPaddle(Paddle paddle)
        {
            // Controls the breadth of collision areas
            var offset = 4;
            var sideBounceDistance = Paddle.Speed * 2;

            var inFront = (Y + Radius >= paddle.Y - offset) && (Y - Radius <= paddle.Y + paddle.Height + offset);
            var isAbove = (Y + Radius >= paddle.Y - offset) && (Y + Radius <= paddle.Y);
            var isBelow = (Y - Radius >= paddle.Y + paddle.Height) && (Y - Radius <= paddle.Y + paddle.Height + offset);

            switch (paddle.Side)
            {
                case Sides.Left:
                    if ((X - Radius >= paddle.X + paddle.Width) && (X - Radius <= paddle.X + paddle.Width + offset))
                    {
                        if (inFront)
                        {
                            X = paddle.Width + Radius + offset;
                            ReverseVelocityX();
                            PlaySound(_soundBounce);
                        }
                    }
                    else if (isAbove && (X - Radius <= paddle.Width + offset))
                    {
                        Y = paddle.Y - Radius - sideBounceDistance;
                        ReverseVelocityY();
                        PlaySound(_soundBounce);
                    }
                    else if (isBelow && (X - Radius <= paddle.Width + offset))
                    {
                        Y = paddle.Y + paddle.Height + Radius + sideBounceDistance;
                        ReverseVelocityY();
                        PlaySound(_soundBounce);
                    }
                    break;

                case Sides.Right:
                    if ((X + Radius >= WindowWidth - paddle.Width - offset) && (X + Radius <= WindowWidth - paddle.Width))
                    {
                        if (inFront)
                        {
                            X = WindowWidth - paddle.Width - Radius - offset;
                            ReverseVelocityX();
                            PlaySound(_soundBounce);
                        }
                    }
                    else if (isAbove && (X + Radius >= WindowWidth - paddle.Width - offset))
                    {
                        Y = paddle.Y - Radius - sideBounceDistance;
                        ReverseVelocityY();
                        PlaySound(_soundBounce);
                    }
                    else if (isBelow && (X + Radius >= WindowWidth - paddle.Width - offset))
                    {
                        Y = paddle.Y + paddle.Height + Radius + sideBounceDistance;
                        ReverseVelocityY();
                        PlaySound(_soundBounce);
                    }
                    break;
            }
        }

        public bool CollidesWithRightSide()
        {
            var collides = (X + Radius >= WindowWidth);

            switch (collides)
            {
                case true:
                    PlaySound(_soundPoint);
                    return true;

                case false:
                    return false;
            }
        }

        public bool CollidesWithLeftSide()
        {
            var collides = (X - Radius <= 0);

            switch (collides)
            {
                case true:
                    PlaySound(_soundPoint);
                    return true;

                case false:
                    return false;
            }
        }

        private void ReverseVelocityX() => VelocityX = -VelocityX;

        private void ReverseVelocityY() => VelocityY = -VelocityY;
    }
}
