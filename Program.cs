﻿using Game.States;
using Raylib_cs;

using static Raylib_cs.Raylib;
using static Game.States.State;
using static Game.GlobalData;

namespace Game
{
    class Program
    {
        static void Main(string[] args)
        {
            GameInit();
            GameLoop();
        }

        private static void GameInit()
        {
            InitWindow(WindowWidth, WindowHeight, WindowTitle);
            InitAudioDevice();

            SetTargetFPS(FPS);
            SetConfigFlags(ConfigFlags.FLAG_MSAA_4X_HINT);

            EnterState(new StartMenuState());
        }

        private static void GameLoop()
        {
            while (!WindowShouldClose())
            {
                CurrentState.Update();
                CurrentState.Draw();
            }

            CloseAudioDevice();
            CloseWindow();
        }
    }
}
