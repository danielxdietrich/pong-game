using Raylib_cs;
using System.IO;

using System.Collections.Generic;
using static Raylib_cs.Raylib;
using static Game.GlobalData;

namespace Game.States
{
    public abstract class State
    {
        public static State CurrentState { get; private set; }
        private static LinkedList<State> StateStack { get; } = new();
        private static Sound _soundMenu = LoadSound(Path.Combine(SoundsPath, "menu.wav"));

        public abstract void Update();
        public abstract void Draw();

        public static void EnterState(State state)
        {
            StateStack.AddLast(state);
            CurrentState = StateStack.Last.Value;

            PlaySound(_soundMenu);
        }

        public static void ExitState()
        {
            var isLastState = (StateStack.Count == 1);

            switch (isLastState)
            {
                case true:
                    CloseWindow();
                    break;

                case false:
                    StateStack.RemoveLast();
                    CurrentState = StateStack.Last.Value;
                    break;
            }

            PlaySound(_soundMenu);
        }

        public static void CleanEnter(State state)
        {
            StateStack.Clear();
            EnterState(state);
        }
    }
}
